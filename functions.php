<?php
	/* Include theme functions */
	$function_dir_path = get_stylesheet_directory();
	$function_file_path = [
		'/libs/enqueue_styles_scripts.php',
	];

	if($function_file_path && is_array($function_file_path)){
		foreach ($function_file_path as $key => $value) {
			if (file_exists($function_dir_path . $value)) {
			    require_once($function_dir_path . $value);
			}
		}
	}

	function custom_password_form() {
		global $post;
		$label = 'pwbox-' . ( empty( $post->ID ) ? rand() : $post->ID );
		$output = '

				<form action="' . esc_url( site_url( 'wp-login.php?action=postpass', 'login_post' ) ) . '" class="form-inline post-password-form" method="post">
					<p>' . __( 'This content is password protected. To view it please enter the password you were provided.' ) . '</p>
					<label for="' . $label . '">' . __( 'Password:' ) . ' <input name="post_password" id="' . $label . '" type="password" size="20" class="form-control" /></label>
					<div class="wp-block-buttons"><div class="wp-block-button"><button type="submit" name="Submit" class="wp-block-button__link has-white-color has-purple-background-color has-text-color has-background has-link-color wp-element-button">' . esc_attr_x( 'Submit', 'post password form' ) . '</button></div></div>
				</form>';
	return $output;
	}

	add_filter('the_password_form', 'custom_password_form', 99);
	remove_filter( 'the_content', 'wpautop' );


	/* end */


?>