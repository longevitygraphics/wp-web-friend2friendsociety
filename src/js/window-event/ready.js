// Windows Ready Handler
(function ($) {

    $(document).ready(function () {
        console.log("ready");
        let isAnimating = false;
        let scrollAmount = 0;
        let p1 = false;
        let p2 = false;
        let p3 = false;

        let textCoverOptions = {
            root: null,
            rootMargin: "15px",
            threshold: 0.1,
        };
        let textCoverOptionsFull = {
            root: null,
            rootMargin: "100px",
            threshold: 1,
        };

        let homeCoverOptions = {
            root: null,
            rootMargin: "0px",
            threshold: 1,
        }

        let footerOptions = {
            root: null,
            rootMargin: "0px",
            threshold: 0.1,
        }

        let textCoverObserver = new IntersectionObserver((e) => {
            console.log("Text Cover Observer Fired");
            console.log(e);
            if (e[0].isIntersecting) {
                $(".down-circle").addClass('draw-up');
            }

        }, textCoverOptions);

        const onScroll = (scale) => {
            scrollAmount++;
            console.log(scrollAmount);
            if (scrollAmount >= (0 * scale) && !p1) {
                console.log("P1 Fades in")
                p1 = true;
                $("#purple").addClass('fade-in');
            }
            if (scrollAmount >= (10 * scale) && !p2) {
                p2 = true;
                console.log("P2 Fades in")
                $("#green").addClass('fade-in');
            }
            if (scrollAmount >= (15 * scale) && !p3) {
                p3 = true;
                console.log("P3 Fades in")
                $("#white").addClass('fade-in');
            }
            if (scrollAmount >= (20 * scale) && p1 && p2 && p3) {
                document.body.style.overflow = 'auto';
                document.removeEventListener('wheel', onScroll);
                document.removeEventListener('touchmove', onScroll);
            }
        };

        let textCoverObserverAnimation = new IntersectionObserver((e) => {
            console.log("Text Cover Animation Observer Fired");
            console.log(e);
            if (e[0].isIntersecting && !isAnimating) {
                isAnimating = true;
                // $(".text-cover")[0].scrollIntoView({ behavior: "instant", block: "center", inline: "center" });
                document.body.style.overflow = 'hidden';
                document.addEventListener('wheel', () => { onScroll(1) });
                document.addEventListener('touchmove', () => { onScroll(1.5) });
            }

        }, textCoverOptionsFull);

        let homeCoverObserver = new IntersectionObserver((e) => {
            console.log("Home Cover Observer Fired");
            console.log(e);
            if (e[0].isIntersecting) {
                $(".down-circle").removeClass('draw-up');
                $(".down-circle").addClass("attention-grab-down");
            } else {
                $(".down-circle").removeClass("attention-grab-down");
            }
        }, homeCoverOptions);


        const handleAnimationEnd = (event) => {
            $(event.target).addClass('animation-complete');
        }
        $("#purple, #green, #white").on('animationend', handleAnimationEnd);

        
        const callback = (entries) => {
            entries.forEach(entry => {
                // console.log(`Percentage Visible:  ${entry.intersectionRatio}`);
                $(".home-cover").find("img").css("opacity", entry.intersectionRatio - 0.25);
            }); 
        };

        const animation = (entries) => {
            entries.forEach(entry => {
                // console.log(`Percentage Visible:  ${entry.intersectionRatio}`);
                let i = entry.intersectionRatio.toFixed(2);
                // console.log(i);

                // $("#purple").css("opacity", i / 3);
                // if ( $("#purple").css("opacity") >= .95) {
                //     $("#green").css("opacity", i);
                // }
                if ( $("#green").css("opacity") >= .75) {
                    $("#white").css("opacity", i );
                }

                if (i >= 0.25) {
                    console.log("P1 Fades in")
                    $("#purple").addClass('fade-in');
                }
                if (i >= .5) {
                    console.log("P2 Fades in")
                    // Check if previous animation is still active
                    if (!$("#purple").hasClass('animation-complete')) {
                        // Delay the addition of the class
                        setTimeout(() => {
                            $("#green").addClass('fade-in');
                        }, 1000); // Adjust 1000 (ms) to match your animation duration
                    } else {
                        $("#green").addClass('fade-in');
                    }
                }
                if (i >= 0.75) {
                    console.log("P3 Fades in")
                    // Repeat the delay logic for P3
                    if (!$("#green").hasClass('animation-complete')) {
                        setTimeout(() => {
                            $("#white").addClass('fade-in');
                        }, 2000); // Adjust the delay as needed
                    } else {
                        $("#white").addClass('fade-in');
                    }
                }
                if (i <= 0.05) {
                    $("#white").removeClass('fade-in animation-complete');
                }
                if (i <= 0.05) {
                    $("#green").removeClass('fade-in animation-complete');
                }
                if (i <= 0.05) {
                    $("#purple").removeClass('fade-in animation-complete');
                }
            }); 
        };

        const buildThresholdList = () => {
            let thresholds = [];
            let numSteps = 100;
          
            for (let i=1.0; i<=numSteps; i++) {
              let ratio = i/numSteps;
              thresholds.push(ratio);
            }
          
            thresholds.push(0);
            return thresholds;
          }
        
        const testOb = new IntersectionObserver(callback, { root: null, rootMargin: "0px", threshold: buildThresholdList() });
        const animOb = new IntersectionObserver(animation, { root: null, rootMargin: "0px", threshold: buildThresholdList() });

        testOb.observe($(".home-cover")[0]);
        animOb.observe($(".text-cover")[0]);

        // $(window).on('scroll', function() {
        //     var element = $(".home-cover");
        //     var distance = element.offset().top - $(window).scrollTop();
        //     console.log("Distance from the top of the viewport: " + distance + "px");
        // });



        let footerObserver = new IntersectionObserver((e) => {
            console.log("foooter Observer Fired");
            console.log(e);
            if (e[0].isIntersecting) {
                $(".down-circle").removeClass('slow-animation');
                $(".down-circle").addClass('flip');
                $(".down-circle").addClass("attention-grab-up");
                $(".down-circle").on("click", () => {
                    window.scrollTo(0,0);
                })
            } else {
                $(".down-circle").addClass('slow-animation');
                $(".down-circle").removeClass('flip');
                $(".down-circle").removeClass("attention-grab-up");
                $(".down-circle").off("click");
                setTimeout( () => {
                    $(".down-circle").removeClass('slow-animation');
                }, 1250);
            }
        }, footerOptions)

        // textCoverObserver.observe($(".text-cover")[0]);
        // textCoverObserverAnimation.observe($(".text-cover")[0]);
        // homeCoverObserver.observe($(".home-cover")[0]);
        // footerObserver.observe($(".site-footer")[0]);

        let cards = document.querySelectorAll(".animated-card");
        cards.forEach(card => {
            card = $(card);
            card.on("click", (e) => {
                let el = $(e.target);
                console.log(el);
                if (el.hasClass(".animated-card")) {
                    el.toggleClass("open");
                } else {
                    el.closest(".animated-card").toggleClass("open");

                }
            })
        })
    });

    let courseButton = document.getElementById("buyAutismCourse");
    let courseLink = document.getElementById("autismCourse");
    courseLink = $(courseLink).find('input[type="image"]')[0];
    $(courseButton).on('click', () => {
        courseLink.click();
    });

    let seminarButton = document.getElementById("buyEarlyChildhood");
    let seminarLink = document.getElementById("earlyChildhood");
    seminarLink = $(seminarLink).find('input[type="image"]')[0];
    $(seminarButton).on('click', () => {
        seminarLink.click();
    });

    let donateButton = document.getElementById("donateButton");
    let donateLink = document.getElementById("donate");
    donateLink = $(donateLink).find('input[type="submit"]')[0];
    $(donateButton).on('click', () => {
        donateLink.click();
    });

}(jQuery));
