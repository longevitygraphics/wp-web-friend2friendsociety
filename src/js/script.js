require('./window-event/ready.js');
require('./window-event/load.js');
require('./window-event/resize.js');
require('./window-event/scroll.js');


const setViewportHeight = () => {
    const vh = window.innerHeight * 0.01;
    document.documentElement.style.setProperty('--vh', `${vh}px`);
}

window.addEventListener('DOMContentLoaded', setViewportHeight);
window.addEventListener('resize', setViewportHeight);
